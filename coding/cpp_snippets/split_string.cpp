#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
	// Complete this function
        vector<int> cont;
        std::stringstream ss(str);
        std::string token;
        while (std::getline(ss, token, ','))
        {
                cont.push_back(stoi(token));
        }
        return cont;
}

int main() {
        string str;
        cin >> str;
        vector<int> integers = parseInts(str);
        for(int i = 0; i < integers.size(); i++)
        {
                cout << integers[i] << "\n";
        }

        return 0;
}
