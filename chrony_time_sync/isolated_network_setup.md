# Master configuration file (/etc/chrony/chrony.conf):

```sh
server vizzavona.intra.cea.fr offline minpoll 8 # CEA NTP server
server 0.debian.pool.ntp.org offline minpoll 8
server 1.debian.pool.ntp.org offline minpoll 8
server 2.debian.pool.ntp.org offline minpoll 8
server 3.debian.pool.ntp.org offline minpoll 8

keyfile /etc/chrony/chrony.keys

commandkey 1

driftfile /var/lib/chrony/chrony.drift

maxupdateskew 100.0

dumponexit

dumpdir /var/lib/chrony

initstepslew 10 192.168.1.102 # list of clients IPs
local stratum 8
manual

allow 10/8
allow 192.168/16 # Clients' subnet
allow 172.16/12

logchange 0.5

rtconutc
```

# Direct client configuration file (/etc/chrony/chrony.conf):

```sh
server 192.168.1.101

keyfile /etc/chrony/chrony.keys

commandkey 24

driftfile /var/lib/chrony/chrony.drift

maxupdateskew 100.0

dumponexit

dumpdir /var/lib/chrony

local stratum 10
initstepslew 20 192.168.1.101

allow 192.168.1.101

logchange 0.5
```