# Building using VS compilator outside VS IDE

## Open VS command prompt 2010 (maybe work with others)

## Go to build directory inside project or solution

Command to build project `AIV-simpleapp` in solution `AIV-Simulator.sln` in parallel (`/m`) in release mode:

```sh
MSBuild AIV-Simulator.sln /m /t:AIV-simpleapp /p:configuration=release
```