import os, re, shutil

episPattern = re.compile(r'([0-9][1-9])[.]([0-9][0-9]) - (.*$)')

for archive in os.listdir('.'):

    if os.path.isfile(archive):

        mo = episPattern.search(archive)

        if mo == None: continue

        season = mo.group(1)
        episode = mo.group(2)
        episName = mo.group(3)

        newName = 'Adventure.Time.S' + season + 'E' + episode + '.' + episName

        newName = newName.replace(' ', '_')

        newName = os.path.join('Season ' + season, newName)
        # print('Renaming "%s" to "%s"...'%(archive, newName))
        # os.makedirs(os.path.dirname(newName), exist_ok=True)
        shutil.move(archive, newName)
