#!/bin/bash

read A
read B
read C

if [[ "$A" -eq "$B" ]]; then
        if [[ "$A" -eq "$C" ]]; then
                echo EQUILATERAL
        else
                echo ISOSCELES
        fi
elif [[ "$B" -eq "$C" ]]; then
        if [[ "$A" -eq "$C" ]]; then
                echo EQUILATERAL
        else
                echo ISOSCELES
        fi
elif [[ "$A" -eq "$C" ]]; then
        if [[ "$B" -eq "$C" ]]; then
                echo EQUILATERAL
        else
                echo ISOSCELES
        fi
else
        echo SCALENE
fi
