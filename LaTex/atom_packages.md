Install `latex` and `pdf-view` packages

```
ctrl-alt-b (build)
ctrl-alt-c (clean)
```

Ignoring build files and output (add to `.gitignore`):

```
**/*.nlo
**/*.nlg
**/*.nls
**/*.pdf
**/*.ps
**/*.aux
**/*.log
**/*.fdb_latexmk
**/*.fls
**/*.bbl
**/*.blg
**/*.idx
**/*.ilg
**/*.ind
**/*.lof
**/*.lot
**/*.out
**/*.synctex.gz
**/*.toc
```
