# Ensure that environment variables like ROS_ROOT and ROS_PACKAGE_PATH are set:

```bash
printenv | grep ROS
```

# Create a ROS Workspace (catkin)

```bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace
```

Build is already possible:

```bash
cd catkin_ws
catkin_make
```

Inside the 'devel' folder you can see that there are now several setup.*sh files. Sourcing any of these files will overlay this workspace on top of your environment.

```bash
source devel/setup.bash
```

Checking:

```bash
echo $ROS_PACKAGE_PATH
```

# Check if tutorials package is installed

```bash
sudo apt-get install ros-<distro>-ros-tutorials
```

# Filesystem Tools

[rospack](http://wiki.ros.org/rospack) allows you to get information about packages.

```bash
rospack find roscpp
```

```bash
roscd [locationname[/subdir]]
roscd roscpp
```

[rosbash](http://wiki.ros.org/rosbash)

Note that roscd, like other ROS tools, will only find ROS packages that are within the directories listed in your ROS_PACKAGE_PATH.

```
workspace_folder/        -- WORKSPACE
  src/                   -- SOURCE SPACE
    CMakeLists.txt       -- 'Toplevel' CMake file, provided by catkin
    package_1/
      CMakeLists.txt     -- CMakeLists.txt file for package_1
      package.xml        -- Package manifest for package_1
    ...
    package_n/
      CMakeLists.txt     -- CMakeLists.txt file for package_n
      package.xml        -- Package manifest for package_n
```