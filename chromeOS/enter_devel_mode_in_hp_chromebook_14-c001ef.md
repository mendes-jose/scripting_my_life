
Developer Mode
==============

Caution: Modifications you make to the system are not supported by Google, may cause hardware, software or security issues and may void warranty.

An unrelated note: Holding just Refresh and poking the Power button hard-resets the machine without entering Recovery. That's occasionally useful, but use it with care - it doesn't sync the disk or shut down politely, so there's a nonzero chance of trashing the contents of your stateful partition.

Introduction
------------

Enabling Developer mode is the first step to tinkering with your Chromebook. With Developer mode enabled you can do things like poke around on a command shell (as root if you want), install Chromium OS, or try other OS's. Note that Developer mode turns off some security features like verified boot and disabling the shell access. If you want to browse in a safer, more secure way, leave Developer mode turned OFF. Note: Switching between Developer and Normal (non-developer) modes will remove user accounts and their associated information from your Chromebook.

Entering
--------

On this device, both the recovery button and the dev-switch have been virtualized. Our partners don't really like physical switches - they cost money, take up space on the motherboard, and require holes in the case.

To invoke Recovery mode, you hold down the **ESC and Refresh (F3) keys and poke the Power button**.

To enter Dev-mode you first invoke Recovery, and at the Recovery screen **press Ctrl-D** (there's no prompt - you have to know to do it). It will ask you to confirm, then reboot into dev-mode.

Dev-mode works the same as always: It will show the scary boot screen and you need to **press Ctrl-D** or wait 30 seconds to continue booting.

USB Boot
--------

By default, USB booting is disabled. Once you are in Dev-mode and have a root shell, you can run:

```bash
sudo crossystem dev_boot_usb=1
```
and reboot once to boot from USB drives **with Ctrl-U**.