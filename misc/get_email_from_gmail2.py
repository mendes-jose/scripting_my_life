#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import imaplib
import getpass
import email
import datetime
import re
import urllib2
import json
import quopri
import unicodedata
import datetime
import csv
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

""" Delivery email example (french version)
--------------------------------------------------
Commande #0000 Livrée
--------------------------------------------------

Livrée à:           2015-10-27 21:50:21
Temps de livraison: 6 min 9 s

Restaurant:         Lotus Blanc
Adresse restaurant: 45 rue de Bourgogne, Paris, 75007
Tél. restaurant:    +33145551889

Client:             Pierre Smith
Adresse client:     00 Rue de Varenne, Paris, 75007
Tél. client:        0555555555
Tél. client 2:      0555555555

Pourboire CB:       0.00
"""

def process_delivery_emails(M):
	# get emails IDs
	rv, msgnums = M.search(None, '(FROM noreply@deliveroo.co.uk SUBJECT "Command #")')
	if rv != 'OK':
		print "No delivery emails found!"
		return

	try:
		with open('./table.csv', 'rb') as csvfile:
			treader = csv.reader(csvfile, delimiter='|')
			#treader = csv.reader(csvfile, delimiter='|',quotechar='',quoting=csv.QUOTE_NONNUMERIC)
			tlist = list(treader)
			hdr = tlist[0]
			table = np.array(tlist[1:])
	except:
		with open('./table.csv', 'w') as csvfile:
			hdr = ['ID', 'T', 'D', 'R', 'AR', 'TR', 'C', 'AC', 'TC1', 'TC2', 'P', 'GP', 'GT', 'GE']
			table_writer = csv.writer(csvfile, delimiter='|')
			#table_writer = csv.writer(csvfile, delimiter='|',quotechar='', quoting=csv.QUOTE_NONNUMERIC)
			table_writer.writerow(hdr)
			table = np.array([[],[]])

	for num in msgnums[0].split():
		rv, data = M.fetch(num, '(RFC822)')
		if rv != 'OK':
			print "ERROR getting email #", num
			return
		
		# quopri decode MINE quoted-printable data
		msg = email.message_from_string(quopri.decodestring(data[0][1]))
		del_info = msg.get_payload()

		m = re.search(r'Commande\s*#([0-9]*)\s*Livrée', del_info)
		n_command_value = int(m.group(1))
		n_command_str = m.group(1)

		#print '---------------', n_command_value, '---------------'

		m = re.search(r'Livrée\s*à:\s*([0-9]*)-([0-9]*)-([0-9]*)\s*([0-9]*):([0-9]*):([0-9]*)', del_info)
		del_moment = datetime.datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)))

		try:
			idx1 = list(np.squeeze(np.where(table[:,hdr.index('ID')] == n_command_str)))
		except IndexError:
			idx1 = []
		except TypeError:
			idx1 = [int(np.squeeze(np.where(table[:,hdr.index('ID')] == n_command_str)))]

		if idx1 != []:
			# print 'TABLE IDX1:', table[idx1, hdr.index('T')]
			# print 'TABLE SPLIT:', [re.split('-| |:', _) for _ in table[idx1, hdr.index('T')]]
			# print 'TABLE DATE TIME:', [datetime.datetime(*[int(a) for a in re.split('-| |:', _)]) for _ in table[idx1, hdr.index('T')]]
			# print 'DEL MOMENT:', del_moment
			# print 'EQ test:', np.where(np.array([datetime.datetime(*[int(a) for a in re.split('-| |:', _)]) for _ in table[idx1, hdr.index('T')]]) == del_moment )
			try:
				idx2 = list(np.squeeze(np.where(np.array([
						datetime.datetime(*[int(a) for a in re.split('-| |:', _)])
						for _ in table[idx1, hdr.index('T')]]) == del_moment)))
			except IndexError:
				idx2 = []
			except TypeError:
				idx2 = [int(np.squeeze(np.where(np.array([
						datetime.datetime(*[int(a) for a in re.split('-| |:', _)])
						for _ in table[idx1, hdr.index('T')]]) == del_moment)))]

			if idx2 != []:
				continue

		#input("PRESS ENTER TO CONTINUE.")

		#try:
		#	same_id_idx = np.squeeze(np.where(table[:,hdr.index('ID') == n_command_value]))
		#	print 'same_id_idx', same_id_idx
		#except IndexError:
		#	same_id_idx = np.array([])
		#if same_id_idx.size > 0:
		#	try:
		#		sii = list(same_id_idx)
		#	except TypeError:
		#		sii = int(same_id_idx)
		#	print sii
		#	same_id_and_time_idx = np.squeeze(np.where([datetime.datetime(*re.split('-| |:', _)) for _ in table[sii, hdr.index('T')]] == del_moment))
		#else:
		#	same_id_and_time_idx = np.array([])

		#if same_id_and_time_idx.size > 0: # the information for this delivery is in the csv table (cache)
		#	if same_id_and_time_idx.size > 1:
		#		print 'ERROR in csv file. Delete it and run the script again.'
		#		return
		#	continue
			#command_idx = int(same_id_and_time_idx[sii])	
			#print '-------------', command_idx
			#print visited_commands
			#print visited_commands[command_idx]

			#if visited_commands[command_idx] == True: # duplicata!!! skip this email
			#	continue
			#else:
			#	visited_commands[command_idx] = True
				#total_del += 1
				#gnum += 1
				#get info from table and not compute again

		#first time we see this delivery

			#total_del += 1
			#gnum += 1

		m = re.search(r'Temps de livraison:\s*([0-9]*)\s*min\s*([0-9]*)\s*s', del_info)
		#accum_del_time += int(m.group(1))*60 + int(m.group(2))
		del_duration = int(m.group(1))*60 + int(m.group(2))

		m = re.search(r'Pourboire CB:\s*([0-9]*)\.([0-9]*)', del_info)
		del_tips = float(m.group(2))/100.+float(m.group(1))

		m = re.search(r'Restaurant:\s*(.*)\r', del_info)
		del_rest = m.group(1)

		m = re.search(r'Adresse restaurant:\s*(.*)\r', del_info)
		rest_addr = m.group(1) # remove \r

		m = re.search(r'Tél. restaurant:\s*([0-9+]*)\r', del_info)
		rest_tel = m.group(1)

		m = re.search(r'Client:\s*(.*)\r', del_info)
		del_clnt = m.group(1)

		m = re.search(r'Tél. client:\s*([0-9+]*)\r', del_info)
		clnt_tel = m.group(1)

		m = re.search(r'Tél. client 2:\s*([0-9+]*)\r', del_info)
		clnt_tel2 = m.group(1)

		m = re.search(r'Adresse client:\s*(.*)\r', del_info)
		clnt_addr = m.group(1) # remove \r

		#call google maps api to cal distance and estimated time between address

		base_url = 'https://maps.googleapis.com/maps/api/directions/json?'
		api_key = 'AIzaSyAPQW4xO9D94A2yAIAdG9JGRjC9-Vtle7A'

		addr_start = rest_addr.replace(" ", "+")
		addr_end = clnt_addr.replace(" ", "+")

		# remove accents
		addr_start = unicodedata.normalize('NFKD', addr_start.decode('utf-8')).encode('ascii', 'ignore')
		addr_end = unicodedata.normalize('NFKD', addr_end.decode('utf-8')).encode('ascii', 'ignore')

		#addr_start = addr_start[0:-1]
		#addr_end = addr_end[0:-1]

		final_url = base_url+'origin='+addr_start+'&destination='+addr_end+'&mode=bicycling&key='+api_key

		response = urllib2.urlopen(final_url)

		gresp = json.loads(response.read())

		try:
			gdist = gresp['routes'][0]['legs'][0]['distance']['value']
			gtime = gresp['routes'][0]['legs'][0]['duration']['value']
			#print 'estimated time:', gtime
			gpossible = True
		except IndexError:
			#gnum -= 1
			gpossible = False
			gdist = 0
			gtime = 0
			print 'Error using google maps api for estimating distance and time'
		
		#add to table
		newline = [n_command_value, str(del_moment), del_duration, del_rest, rest_addr,  rest_tel, del_clnt, clnt_addr, clnt_tel,
					clnt_tel2, del_tips, gpossible, gtime, gdist]

		print 'table shape', table.shape
		print 'newline shape', np.array([newline]).shape
		if table.shape[1] != np.array([newline]).shape[1]:
			table = np.array([newline])
		else:
			table = np.vstack((table, np.array([newline])))
		with open('./table.csv', 'a') as csvfile:
			table_writer = csv.writer(csvfile, delimiter='|')
			#table_writer = csv.writer(csvfile, delimiter='|',quotechar='', quoting=csv.QUOTE_NONNUMERIC)
			table_writer.writerow(newline)


	#print 'Total number of deliveries:', total_del

	#print 'Total delivery time:', int(accum_del_time/60), 'min', accum_del_time - int(accum_del_time/60)*60, 's'

	#tavg_sec = int(round(accum_del_time/float(total_del)))

	#print 'Average delivery time:', int(tavg_sec/60), 'min', tavg_sec - int(tavg_sec/60)*60, 's'

	#print 'Total delivery time (estimated with google maps api):', int(accum_del_gtime/60), 'min', accum_del_gtime - int(accum_del_gtime/60)*60, 's'

	#tgavg_sec = int(round(accum_del_gtime/float(gnum)))

	#print 'Average arrival time (estimated with google maps api):', int(tgavg_sec/60), 'min', tgavg_sec - int(tgavg_sec/60)*60, 's'

	#print 'Total tips by card:', accum_del_tips

	#print 'Average tips by card:', round(accum_del_tips/total_del, 2)

	#print 'List of restaurants:', restaurants
	#print 'Restaurants:'
	#for restaurant in restaurants:
	#	print '\t', restaurant

	#print ''


def print_sumary():
	with open('./table.csv', 'rb') as csvfile:
			treader = csv.reader(csvfile, delimiter='|')
			#treader = csv.reader(csvfile, delimiter='|',quotechar='',quoting=csv.QUOTE_NONNUMERIC)
			tlist = list(treader)
			hdr = tlist[0]
			table = np.array(tlist[1:])

	total_orders = table.shape[0]
	print '{0:48} {1:16}'.format(
			'Total number of deliveries:',
			total_orders)

	total_time = sum([int(tim) for tim in table[:, hdr.index('D')]])
	print '{0:48} {1:4} h {2:2} m {2:2} s'.format(
			'Total delivery time:',
			int(total_time/3600),
			int(total_time/60) - int(total_time/3600)*3600/60,
			total_time - int(total_time/60)*60)

	tavg_sec = int(round(total_time/float(total_orders)))
	print '{0:48}        {1:2} m {2:2} s'.format(
			'Average delivery time:',
			int(tavg_sec/60),
			tavg_sec - int(tavg_sec/60)*60)

	gtotal_time = sum([int(tim) for tim in table[:, hdr.index('GT')]])

	try:
		gp_idx = list(np.squeeze(np.where(table[:,hdr.index('GP')] == 'True')))
	except IndexError:
		gp_idx = []
	except TypeError:
		gp_idx = [int(np.squeeze(np.where(table[:,hdr.index('GP')] == 'True')))]

	gtotal = len(gp_idx)

	print '{0:48} {1:4} h {2:2} m {2:2} s'.format(
			'Total delivery time (by google maps api):',
			int(gtotal_time/3600),
			int(gtotal_time/60) - int(gtotal_time/3600)*3600/60,
			gtotal_time- int(gtotal_time/60)*60)

	gtavg_sec = int(round(gtotal_time/float(gtotal)))
	print '{0:48}        {1:2} m {2:2} s'.format(
			'Average arrival time (by google maps api):',
			int(gtavg_sec/60),
			gtavg_sec - int(gtavg_sec/60)*60)

	gtotal_dist = sum([int(dist) for dist in table[:, hdr.index('GE')]])

	print '{0:48} {1:14} m'.format(
			'Total delivery distance (by google maps api):',
			gtotal_dist)

	gdavg = int(round(gtotal_dist/float(gtotal)))
	print '{0:48} {1:14} m'.format(
			'Average arrival distance (by google maps api):',
			gdavg)

	total_tips = sum([float(tip) for tip in table[:, hdr.index('P')]])
	print '{0:48} {1:12.2} EUR'.format(
			'Total tips by card:',
			total_tips)

	print '{0:48} {1:12.2} EUR'.format(
			'Average tip by card:',
			total_tips/total_orders)

	sort_orders_idx = table[:, hdr.index('T')].argsort()

	fig = plt.figure()
	ax = fig.gca()
	plt.hold(True)
	plt.grid()
	ax.plot(range(total_orders), table[sort_orders_idx, hdr.index('D')], label='Actual time', marker='o', color='blue')
	ax.fill_between(np.arange(total_orders), 0, [int(v) for v in table[sort_orders_idx, hdr.index('D')]], alpha=0.2, facecolor='blue')
	
	ax.plot(range(total_orders), table[sort_orders_idx, hdr.index('GT')], label='Google time', marker='o', color='red')
	ax.fill_between(np.arange(total_orders), 0, [int(v) for v in table[sort_orders_idx, hdr.index('GT')]], alpha=0.2, facecolor='red')
	#ax.plot(range(total_orders), table[sort_orders_idx, hdr.index('GT')], marker='--')
	ax.set_xlabel('Deliveries')
	ax.set_ylabel('Time (s)')
	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels)
	plt.show()

	print ''


M = imaplib.IMAP4_SSL('imap.gmail.com')

try:
    M.login('jmagno.mf', getpass.getpass())
except imaplib.IMAP4.error:
    print "LOGIN FAILED!!!"
    # ... exit or deal with failure...


rv, data = M.select()
if rv == 'OK':
	print "Processing emails...\n"

	process_delivery_emails(M)

	print_sumary()
	
	M.close()
M.logout()