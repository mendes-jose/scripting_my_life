# OBS zlib fix

I had this too and found out how to fix it, copying all the dll's files from "obs-studio\bin\32bit" to "obs-studio\data\obs-plugins\obs-ffmpeg" and it works now.

Uninstalling the program (and making sure there is no "AppData\Roaming\obs-studio") and reinstalling it,(in Programs Files or other place) did not work.

Using obs32.exe (OBS 0.13.2) ( Windows 10 x64) and clicking "Start Recording":
(ERROR: ffmpeg-mux32.exe - Entry Point Not Found; The Procedure entry point zlibCompileFlags could not be located in dynamic link library C:\Program Files (x86)\obs-studio\bin\32bit\avform-57.dll.)

and

(ERROR: ffmpeg-mux32.exe - Entry Point Not Found; The Procedure entry point deflateBound could not be located in dynamic link library C:\Program Files (x86)\obs-studio\bin\32bit\avform-57.dll.)

OBS-Studio shows down "Encoding overloaded!", there is no ffmpeg-mux32.exe in Task Manager, no video file written.

-----------------------------------------------------------------------------------------------------------
Without copying the dll's(without the fix)(default installation):

Clicking "Start Streaming" works, video is shown on Twitch.

Setting Output Mode to Advanced and selecting Recording>Type>Custom Output(FFmpeg) and clicking "Start Recording" works.

Setting Output Mode to Simple and clicking "Start Recording" does not work.