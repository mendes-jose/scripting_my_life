void staircase(int n) {
        // rows iterations
        for (auto i = 0; i < n; ++i)
        {
                // space iterations
                for (auto j = 0; j < n - i - 1; ++j)
                {
                        std::cout << " ";
                }
                // hashtag iterations
                for (auto j = 0; j < i + 1; ++j)
                {
                        std::cout << "#";
                }
                std::cout << std::endl;
        }
}
