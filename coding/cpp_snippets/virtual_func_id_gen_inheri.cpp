#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Person
{
protected:
        string name_;
        int age_;
// int next_id_;
public:
        Person() {
        }
        virtual void getdata()
        {
                cin >> name_ >> age_;
        };
        virtual void putdata()
        {
                cout << name_ << ' ' << age_;
        };
};
// int Person::next_id_ = 0;

class Student : public Person
{
private:
        static const size_t n_marks_ = 6;
        int marks_[n_marks_];
        int cur_id_;
        static int next_id_;
public:
        Student() : Person(), cur_id_(++next_id_) {
        }
        virtual void getdata()
        {
                cin >> name_ >> age_;
                for (auto i = 0; i < n_marks_; ++i)
                {
                        cin >> marks_[i];
                }
        };
        virtual void putdata()
        {
                int sum_marks = 0;
                for (auto mark : marks_)
                {
                        sum_marks += mark;
                }
                cout << name_ << ' ' << age_ << ' ' << sum_marks << ' ' << cur_id_ << endl;
        };
};

int Student::next_id_ = 0;

class Professor : public Person
{
private:
        int publications_;
        int cur_id_;
        static int next_id_;
public:
        Professor() : Person(), cur_id_(++next_id_) {
        }
        virtual void getdata()
        {
                cin >> name_ >> age_ >> publications_;
        };
        virtual void putdata()
        {
                cout << name_ << ' ' << age_ << ' ' << publications_ << ' ' << cur_id_ << endl;
        };
};

int Professor::next_id_ = 0;

int main(){

        int n, val;
        cin>>n; //The number of objects that is going to be created.
        Person *per[n];

        for(int i = 0; i < n; i++) {

                cin>>val;
                if(val == 1) {
                        // If val is 1 current object is of type Professor
                        per[i] = new Professor;

                }
                else per[i] = new Student; // Else the current object is of type Student

                per[i]->getdata(); // Get the data from the user.

        }

        for(int i=0; i<n; i++)
                per[i]->putdata(); // Print the required output for each object.

        return 0;

}
