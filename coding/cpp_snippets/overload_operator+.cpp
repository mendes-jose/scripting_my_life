// Operator Overloading

class Matrix
{
public:
        Matrix(){};
        ~Matrix(){};

        vector< vector<int> > a;

        friend Matrix operator+(Matrix lhs, // passing lhs by value helps optimize chained a+b+c
                         const Matrix& rhs)
        {
                size_t rows = lhs.a.size();
                size_t cols = lhs.a[0].size();
                for(auto i = 0; i < rows; ++i)
                {
                       for (auto j = 0; j < cols; ++j)
                       {
                               lhs.a[i][j] += rhs.a[i][j]; // reuse lhs for storing result
                       }
                }
                return lhs;
        };
};
// Matrix operator+(Matrix lhs, // passing lhs by value helps optimize chained a+b+c
//                          const Matrix& rhs)
// {
//         size_t rows = lhs.a.size();
//         size_t cols = lhs.a[0].size();
//         for(auto i = 0; i < rows; ++i)
//         {
//                for (auto j = 0; j < cols; ++j)
//                {
//                        lhs.a[i][j] += rhs.a[i][j]; // reuse lhs for storing result
//                }
//         }
//         return lhs;
// };
int main () {
        int cases,k;
        cin >> cases;
        for(k=0; k<cases; k++) {
                Matrix x;
                Matrix y;
                Matrix result;
                int n,m,i,j;
                cin >> n >> m;
                for(i=0; i<n; i++) {
                        vector<int> b;
                        int num;
                        for(j=0; j<m; j++) {
                                cin >> num;
                                b.push_back(num);
                        }
                        x.a.push_back(b);
                }
                for(i=0; i<n; i++) {
                        vector<int> b;
                        int num;
                        for(j=0; j<m; j++) {
                                cin >> num;
                                b.push_back(num);
                        }
                        y.a.push_back(b);
                }
                result = x+y;
                for(i=0; i<n; i++) {
                        for(j=0; j<m; j++) {
                                cout << result.a[i][j] << " ";
                        }
                        cout << endl;
                }
        }
        return 0;
}
