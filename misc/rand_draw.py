from random import shuffle

def tickets_sorting():
    names = [ "Rodrigo", "Caio", "Leo", "Bruno", "Beijinho", "Romeu" ]
    shuffle( names )
    print "Bilhetes do Rodrigo:\n1) " + names[0] + "\n2) " + names[1] + "\n"
    print "Bilhetes do Romeu (Cadeiras do alto):\n1) " + names[2] + "\n2) " + names[3] + "\n"
    print "Bilhetes do Romeu (Cadeiras de baixo):\n1) " + names[4] + "\n2) " + names[5] + "\n"

tickets_sorting()
