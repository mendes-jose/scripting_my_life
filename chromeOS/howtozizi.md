Ligue o PC (ele liga sozinho quando é aberto)
=============================================

Faça login no Crhome OS
=======================

Use sua sua senha Google.

Abra uma guia `crosh`
=====================

Indendente do Google Chrome estiver aberto ou não, apenas aperte `Ctrl + Alt + t`.
Uma guia `crosh` vai aparecer.

Abra o `shell`
==============

Na guia `crosh` digite `shell` e aperte `enter`.

Inicie Xubuntu (o linux)
========================

Para iniciar o linux digite:

```bash
sudo startxfce4 -n zizi
```

Ignore tudo que aparecer inclusive os erros em vermelho e espere até que o linux apareça.

![alt text](https://gitlab.com/mendes-jose/scripting_my_life/raw/master/chromeOS/images/shell.png)

Procurar uma aplicação
======================

`Alt + F3` e comece a digitar o nome da aplicação.

Atalhos para algumas aplicações já estão no Desktop.

Outros atalhos
==============

`Alt + Shift`: trocar o tipo de teclado (fr-fr, pt-br, en-us)

`Alt + Tab`: trocar a janela em foco

`Ctrl + q`: fechar uma janela (nesse caso a tecla `q` vai ser a tecla `a` sempre, independente do tipo de teclado escolhido, assim, olhando para o teclado será sempre `Ctrl + a`)

`Ctrl + Alt + d`: minimizar todoas as janelas e mostrar o Desktop

`Ctrl + Alt + ->`: ir para o próximo Desktop

`Ctrl + Alt + <-`: ir para Desktop precedente

`Ctrl + Alt + Home`: levar a janela em foco para o próximo Desktop (Home no teclado fr é uma seta na diagonal apontando para noroeste)

`Ctrl + Alt + End`: levar a janela em foco para o Desktop precedente (End no teclado fr é `fin`)

`Ctrl + Alt + t`: abrir o terminal lindo maravilhoso

`F4`: tira a janela do linux do modo "fullscreen" mostrando a barra do Chrome OS ou, se apertar de novo, põe a janela em "fullscreen"

Configurações
=============

Tem um atalho para o "Settings Manager" no Desktop que pode ser útil.

Para sair/desligar
==================

Para encerrar o linux você deve clicar em zizi no alto direito da tela, depois em "Log Out..." e de novo em "Log Out".

Depois do log out o shell do Chrome OS vai aparecer com algumas coisas escritas na tela. Você pode ignorar tudo e, depois de alguns segundos, pode desligar o PC **apertando e segurando** o "Power Button"
