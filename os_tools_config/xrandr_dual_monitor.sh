#!/bin/bash

# Check monitors names and res with
# xrandr

# RESOLUTION SETTINGS
# This sets your VGA1 monitor to its best resolution.
xrandr --output VGA-0 --mode 1920x1080 --rate 60
# This sets your laptop monitor to its best resolution.
xrandr --output VGA-1 --mode 1920x1080 --rate 60

# MONITOR ORDER
# Put the Laptop right, VGA1 monitor left
# xrandr --output VGA1 --left-of LVDS1
# Put the Laptop left, VGA1 monitor right
xrandr --output VGA-0 --left-of VGA-1

# PRIMARY MONITOR
# This sets your laptop monitor as your primary monitor.
xrandr --output VGA-0 --primary
# This sets your VGA monitor as your primary monitor.
# xrandr --output VGA1 --primary