// Complete the climbingLeaderboard function below.
vector<int> climbingLeaderboard(vector<int> scores, vector<int> alice) {

        vector<int> ranks(scores.size());
        ranks[0] = 1;

        for (auto i = 1; i < scores.size(); ++i)
        {
                if (scores[i] == scores[i-1])
                {
                        ranks[i] = ranks[i-1];
                }
                else
                {
                        ranks[i] = ranks[i-1] + 1;
                }
        }

        vector<int> alice_ranks(alice.size());
        size_t score_board_lim = scores.size();

        for (auto i = 0; i < alice.size(); ++i)
        {
                alice_ranks[i] = 1;

                size_t j = score_board_lim;
                while (j-- > 0)
                {
                        if (alice[i] == scores[j])
                        {
                                alice_ranks[i] = ranks[j];
                                break;
                        }
                        else if (alice[i] < scores[j])
                        {
                                alice_ranks[i] = ranks[j] + 1;
                                break;
                        }
                }
                score_board_lim = j + 1;
        }
        return alice_ranks;
}
