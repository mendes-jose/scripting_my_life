#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <limits>
using namespace std;


int main() {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        size_t n, q;

        cin >> n;
        cin >> q;

        vector< vector<int> > arr(n);

        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        for (auto i = 0; i < n; ++i)
        {
                size_t siz;
                cin >> siz;
                for (auto j = 0; j < siz; ++j)
                {
                        int aux;
                        cin >> aux;
                        arr[i].push_back(aux);
                }

                // for reading line as a list of unknown number of elements
                // it do not work it cin.ignore from before is not present
                // string str;
                // if(getline(cin, str))
                // {
                //         cout << "[" << i << "] " << str << endl;
                //         stringstream ss(str);
                //         int v;
                //         while(ss >> v)
                //                 arr[i].push_back(v);
                // }
        }

        for (auto i = 0; i < q; ++i)
        {
                size_t r, c;
                cin >> r;
                cin >> c;
                cout << arr[r][c] << endl;
        }
        return 0;
}
