# filesystem information

```sh
df -h
```

# Others

```sh
lsblk
fdisk -l
```

# Test speed

```sh
sudo hdparm -Tt /dev/sda
```