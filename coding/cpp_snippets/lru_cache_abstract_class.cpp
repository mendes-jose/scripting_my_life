#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <set>
#include <cassert>
using namespace std;

struct Node {
        Node* next;
        Node* prev;
        int value;
        int key;
        Node(Node* p, Node* n, int k, int val) : prev(p),next(n),key(k),value(val){
        };
        Node(int k, int val) : prev(NULL),next(NULL),key(k),value(val){
        };
};

class Cache {

protected:
        map<int,Node*> mp;    //map the key to the node in the linked list
        int cp;     //capacity
        Node* tail;    // double linked list tail pointer
        Node* head;    // double linked list head pointer
        virtual void set(int, int) = 0;    //set function
        virtual int get(int) = 0;    //get function

};


class LRUCache : Cache
{
private:
        void sort_after_cache_hit_(int k, int v)
        {
                // check if it is already the most recent used key
                if (mp[k]->prev != NULL)
                {
                        // if it is not the last either
                        if (mp[k]->next != NULL)
                        {
                                // save current prev and next of mp[k]
                                Node* n_prev = mp[k]->prev;
                                Node* n_next = mp[k]->next;

                                // put mp[k] at the begining
                                mp[k]->next = head;
                                head = mp[k];
                                mp[k]->prev = NULL;

                                // redo the link where we removed mp[k]
                                n_prev->next = n_next;
                                n_next->prev = n_prev;
                        }
                        else         // node is the last
                        {
                                // save current prev and next of mp[k]
                                Node* n_prev = mp[k]->prev;

                                // put mp[k] at the begining
                                mp[k]->next = head;
                                head = mp[k];
                                mp[k]->prev = NULL;

                                // redo the link where we removed mp[k]
                                n_prev->next = NULL;
                        }
                }
        }
public:
        LRUCache(int cap) : Cache()
        {
                cp = cap;
                tail = NULL;
                head = NULL;
        };

        virtual void set(int k, int v)
        {
                // cout << "set(" << k << ", " << v << ")" << endl;
                // search for k in map
                if (mp.find(k) != mp.end())
                {
                        // cout << "cache hit!" << endl;
                        // cache hit!

                        // should update value of key and rearange so it becomes the most recent
                        mp[k]->value = v;
                        // mp[k]->key = k;

                        // cout << "call sort" << endl;
                        sort_after_cache_hit_(k, v);
                        // cout << "after sort" << endl;
                }
                else
                {
                        // cache miss
                        // cout << "cache miss :(" << endl;

                        // check capacity
                        if (mp.size() + 1 > cp)
                        {
                                // cache reached its capacity
                                // cout << "cache is full :(" << endl;

                                // remove least recently used key on map
                                mp.erase(tail->key);
                                // cout << "After erase" << endl;

                                // add new key and point to tail
                                mp[k] = tail;

                                // update last node with new key and value
                                tail->value = v;
                                tail->key = k;

                                // put last node at the begining
                                // cout << "call sort" << endl;
                                sort_after_cache_hit_(k, v);
                                // cout << "after sort" << endl;
                        }
                        else
                        {
                                // cout << "cache not full!" << endl;
                                // cout << "creat: Node(" << k << ", " << v << ")" << endl;
                                // no problem inserting
                                mp[k] = new Node(NULL, head, k, v);
                                if (head != NULL)
                                {
                                        // cout << "  its next: Node(" << head->key << ", " << head->value << ")" << endl;
                                        head->prev = mp[k];
                                }
                                if (tail == NULL)
                                {
                                        tail = mp[k];
                                }
                                head = mp[k];
                        }
                }
        };
        virtual int get(int k)
        {
                if (mp.find(k) != mp.end())
                {
                        return mp[k]->value;
                }
                return -1;
        };
};

int main() {
        int n, capacity,i;
        cin >> n >> capacity;
        LRUCache l(capacity);
        for(i=0; i<n; i++) {
                string command;
                cin >> command;
                if(command == "get") {
                        int key;
                        cin >> key;
                        cout << l.get(key) << endl;
                }
                else if(command == "set") {
                        int key, value;
                        cin >> key >> value;
                        l.set(key,value);
                }
        }
        return 0;
}
