#via a simple regex:
def parseInt(sin):
    import re
    m = re.search(r'^(\d+)[.,]?\d*?', str(sin))
    return int(m.groups()[-1]) if m and not callable(sin) else None

def _idColorForImage(text):
    """ Got from https://github.com/zenozeng/color-hash/blob/master/dist/color-hash.js

    Parameters
    ----------
    text : type
        text to hash.

    Returns
    -------
    type
        A string containing the color in hex format, e.g. "#c2cf12".

    """

    seed = 131
    seed2 = 137
    mhash = 0
    text += 'x'

    MAX_SAFE_INTEGER = int(0xffffff) / seed2

    for i in range(len(text)):

        if mhash > MAX_SAFE_INTEGER:
            mhash = parseInt(mhash / seed2)

        mhash = mhash * seed + ord(text[i])

    hexrepr = hex(mhash)[2:]
    while len(hexrepr) < 6:
        hexrepr = '0' + hexrepr

    return '#' + hexrepr

print(_idColorForImage('df;skldasdfsgj'))
