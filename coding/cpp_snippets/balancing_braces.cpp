#include <vector>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <limits>

using namespace std;

#include <chrono>

vector<string> check_braces1(vector<string> values)
{
        vector<string> resp(values.size());

        size_t max_siz = 0;
        for (auto el : values)
        {
                if (max_siz < el.size())
                {
                        max_siz = el.size();
                }
        }
        vector<char> stack(max_siz/2);
        // for (auto str : values)
        for (auto i = 0; i < values.size(); ++i)
        {
                size_t idx = 0;
                resp[i] = "";
                for (char& c : values[i])
                {
                        if (c == '(' || c == '{' || c == '[')
                        {
                                if (idx >= stack.size())
                                {
                                        resp[i] = "NO";
                                        break;
                                }
                                stack[idx++] = c;
                        }
                        else
                        {
                                if (idx == 0)
                                {
                                        resp[i] = "NO";
                                        break;
                                }
                                else
                                {
                                        if (c == ')' && stack[idx-1] == '(')
                                        {
                                                idx--;
                                        }
                                        else if (c == ']' && stack[idx-1] == '[')
                                        {
                                                idx--;
                                        }
                                        else if (c == '}' && stack[idx-1] == '{')
                                        {
                                                idx--;
                                        }
                                        else
                                        {
                                                resp[i] = "NO";
                                                break;
                                        }
                                }
                        }
                }
                if (resp[i] == "" && idx == 0)
                {
                        resp[i] = "YES";
                }
                else
                {
                        resp[i] = "NO";
                }
        }
        return resp;
}

vector<string> check_braces2(vector<string> values)
{
        vector<string> resp(values.size());

        // size_t max_siz = 0;
        // for (auto el : values)
        // {
        //         if (max_siz < el.size())
        //         {
        //                 max_siz = el.size();
        //         }
        // }
        // for (auto str : values)
        for (auto i = 0; i < values.size(); ++i)
        {
                vector<char> stack(values[i].size()/2);
                size_t idx = 0;
                resp[i] = "";
                for (char& c : values[i])
                {
                        if (c == '(' || c == '{' || c == '[')
                        {
                                if (idx >= stack.size())
                                {
                                        resp[i] = "NO";
                                        break;
                                }
                                stack[idx++] = c;
                        }
                        else
                        {
                                if (idx == 0)
                                {
                                        resp[i] = "NO";
                                        break;
                                }
                                else
                                {
                                        if (c == ')' && stack[idx-1] == '(')
                                        {
                                                idx--;
                                        }
                                        else if (c == ']' && stack[idx-1] == '[')
                                        {
                                                idx--;
                                        }
                                        else if (c == '}' && stack[idx-1] == '{')
                                        {
                                                idx--;
                                        }
                                        else
                                        {
                                                resp[i] = "NO";
                                                break;
                                        }
                                }
                        }
                }
                if (resp[i] == "" && idx == 0)
                {
                        resp[i] = "YES";
                }
                else
                {
                        resp[i] = "NO";
                }
        }
        return resp;
}

int main()
{
        vector<string> values;
        int n;
        cin >> n;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        for (auto i = 0; i < n; ++i)
        {
                string aux;
                getline(cin, aux);
                values.push_back(aux);
        }
        chrono::steady_clock::time_point begin = chrono::steady_clock::now();
        vector<string> resp1 = check_braces1(values);
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        cout << "Time difference = " << chrono::duration_cast<chrono::microseconds> (end - begin).count() <<endl;
        cout << "Time difference = " << chrono::duration_cast<chrono::nanoseconds> (end - begin).count() <<endl;

        begin = chrono::steady_clock::now();
        vector<string> resp2 = check_braces2(values);
        end = chrono::steady_clock::now();
        cout << "Time difference = " << chrono::duration_cast<chrono::microseconds> (end - begin).count() <<endl;
        cout << "Time difference = " << chrono::duration_cast<chrono::nanoseconds> (end - begin).count() <<endl;

        // for (auto i = 0; i < resp.size(); ++i)
        // {
        //         cout << resp[i] << endl;
        // }
}
