#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <map>
#include <deque>
#include <sstream>

using namespace std;

typedef struct TagContent
{
        map<string, string> attributes; // dictionary of attributes' keys and their values
        map<string, TagContent*> tags;  // dictionary of nested tags
} TagContent;

template <class Container>
void split_str(const std::string& str, Container& cont, char delim = ' ')
{
        std::stringstream ss(str);
        std::string token;
        while (std::getline(ss, token, delim))
        {
                cont.push_back(token);
        }
}

pair<string, TagContent*> parse_tag(size_t *n_lines, string first_line = "")
{
        string str;

        string tag_name;
        TagContent *tag_content;

        if (first_line == "")
                getline(cin, str);
        else
                str = first_line;
        if (str != "")
        {
                tag_content = new TagContent();

                deque<string> words;
                split_str(str.substr(1, str.size()-2), words);

                tag_name = words[0];
                size_t i = 1;
                while (i < words.size())
                {
                        tag_content->attributes[words[i]] = words[i+2].substr(1, words[i+2].size()-2);
                        i += 3;
                }
        }
        else
        {
                throw "Cannot getline!";
        }

        while(true)
        {
                if(getline(cin, str))
                {
                        string aux("</" + tag_name + ">");
                        if(str == aux)
                        {
                                *n_lines += 2;
                                break;
                        }
                        else
                        {
                                tag_content->tags.insert(parse_tag(n_lines, str));
                        }
                }
                else
                {
                        throw "Cannot getline!";
                }
        }
        return std::make_pair (tag_name, tag_content);
}

void print_query(string query, TagContent root)
{
        deque<string> list_tags;
        split_str(query, list_tags, '.');

        TagContent document = root;
        while (true)
        {
                if (list_tags.size() == 1)
                {
                        vector<string> aux;
                        split_str(list_tags[0], aux, '~');
                        string t_name(aux[0]);
                        string attr(aux[1]);

                        if (document.tags.find(t_name) != document.tags.end())
                        {
                                if (document.tags[t_name]->attributes.find(attr) != document.tags[t_name]->attributes.end())
                                {
                                        cout << document.tags[t_name]->attributes[attr] << endl;
                                        break;
                                }
                                cout << "Not Found!" << endl;
                                break;
                        }
                        cout << "Not Found!" << endl;
                        break;
                }

                if (document.tags.find(list_tags[0]) != document.tags.end())
                {
                        document = *(document.tags[list_tags[0]]);
                        list_tags.pop_front();
                        continue;
                }
                cout << "Not Found!" << endl;
                break;
        }
}

void clear_memory(TagContent root)
{
        if (root.tags.empty())
        {
                return;
        }

        for(auto el : root.tags)
        {
                // cout << "go clear: " << el.first << endl;
                clear_memory(*(el.second));
                delete el.second;
                // cout << el.first << " is now clear" << endl;
        }
}

int main() {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        size_t N, Q;
        cin >> N;
        cin >> Q;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        TagContent root;

        size_t i = 0;
        while(i < N)
        {
                root.tags.insert(parse_tag(&i));
        }

        for(auto i = 0; i < Q; ++i)
        {
                string str;
                if(getline(cin, str))
                {
                        print_query(str, root);
                }
        }
        clear_memory(root);
        return 0;
}
