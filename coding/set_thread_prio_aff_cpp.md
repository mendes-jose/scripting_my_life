# C++ set thread priority and affinity

```cpp
#include <unistd.h>
#include <syscall.h>
#include <cstring>
void set_thread_params(int prio, int affinity, const std::string &name)
{
        pthread_t thId = pthread_self();
        sched_param sch_params;
        sch_params.sched_priority = prio;
        cpu_set_t mask;
        CPU_ZERO(&mask);
        CPU_SET(affinity, &mask);
        if (pthread_setschedparam(thId, SCHED_FIFO, &sch_params))
        {
                std::cout << "Failed to set Thread scheduling of " << name << " ("
                          << (pid_t) syscall(SYS_gettid) << "), error "
                          << std::strerror(errno) << std::endl;
        }
        else 
        {
                std::cout << "Thread set to priority of " << name << " ("
                          << (pid_t) syscall(SYS_gettid) << "), value " << prio
                          << std::endl;
        }
        if (pthread_setaffinity_np(thId, sizeof(cpu_set_t), &mask))
        {
                std::cout << "Failed to set Thread affinity of " << name << " ("
                          << (pid_t) syscall(SYS_gettid) << "), error "
                          << std::strerror(errno) << std::endl;
        }
        else
        {
                std::cout << "Thread set to affinity of " << name << " ("
                          << (pid_t) syscall(SYS_gettid) << "), value " << affinity
                          << std::endl;
        }
}
```

- `prio` est la priorité qui va de 1 (faible) à 99 (maximale).
- `affinity` est « sur quel cœur tu veux que ça s’exécute ». 0 est le premier, 1 le second, 2 le troisième ... enfin tu as compris quoi.
- `name` est le nom de la tâche. Je le met pour que ça soit plus clair pour moi dans les logs, mais si tu préfères l’enlever, vas-y.

# Enable user usage of `SCHED_FIFO` and `SCHED_RR`

```sh
sudo groupadd realtime
sudo usermod -aG realtime <user_name>
echo "@realtime   soft  cpu       unlimited" | sudo tee -a /etc/security/limits.conf
echo "@realtime   -     rtprio    100" | sudo tee -a /etc/security/limits.conf
echo "@realtime   -     memlock   unlimited" | sudo tee -a /etc/security/limits.conf
```

And restart the machine!

# Logging

[LTTng](https://lttng.org/)
