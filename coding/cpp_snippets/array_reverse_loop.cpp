#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        const size_t max_N = 1000;
        int arr[max_N];

        size_t N;
        cin >> N;
        if (N > max_N)
        {
                throw "Number of integers out of limits!";
        }

        for (auto i = 0; i < N; ++i)
                cin >> arr[i];
        for (auto i = N; i-- > 0;)
                cout << arr[i] << ' ';
        return 0;
}
