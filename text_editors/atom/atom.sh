#!/bin/sh

set -e

sudo apt-get install python3-pip -y

sudo python3 -m pip install autopep8
sudo python3 -m pip install yapf

sudo apt-get install uncrustify -y

apm install atom-beautify minimap Zen busy-signal language-cmake docblockr switch-header-source language-latex pdf-view terminal-tab

apm install --production file-icons

echo "'atom-text-editor':\n\t'alt-shift-i': 'editor:split-selections-into-lines'\n\t'ctrl-alt-e': 'atom-beautify:beautify-editor''\n\t'ctrl-\`': 'terminal:open'" >> ~/.atom/keymap.cson

cp config.cson ~/.atom/
cp uncrustify.cfg /home/mnds/.atom/packages/atom-beautify/src/beautifiers/uncrustify/uncrustify.cfg
