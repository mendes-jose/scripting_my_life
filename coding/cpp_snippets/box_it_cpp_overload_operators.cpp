#include <bits/stdc++.h>

using namespace std;

//Implement the class Box
//l,b,h are integers representing the dimensions of the box

// The class should have the following functions :

// Constructors:
// Box();
// Box(int,int,int);
// Box(Box);


// int getLength(); // Return box's length
// int getBreadth (); // Return box's breadth
// int getHeight ();  //Return box's height
// long long CalculateVolume(); // Return the volume of the box

//Overload operator < as specified
//bool operator<(Box& b)

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)

class Box
{
public:
        Box(): l_(0), b_(0), h_(0) {};
        Box (int l, int b, int h): l_(l), b_(b), h_(h) {};
        Box(const Box& bcy): l_(bcy.l_), b_(bcy.b_), h_(bcy.h_) {};
        inline int getLength() { return l_; }; // Return box's length
        inline int getBreadth () { return b_; }; // Return box's breadth
        inline int getHeight () { return h_; };  //Return box's height
        long long CalculateVolume()
        {
                long long vol = 1;
                vol *= l_;
                vol *= b_;
                vol *= h_;
                return vol;
        }; // Return the volume of the box
        /*
           It's better to be symetric having a friend meethod with lhs and rhs arguments
        */
        friend bool operator<(const Box& lhs, const Box& rhs)
        {
                if (lhs.l_ < rhs.l_) {
                        return true;
                }
                else if (lhs.l_ == rhs.l_) {
                        if (lhs.b_ < rhs.b_) {
                                return true;
                        }
                        else if (lhs.b_ == rhs.b_) {
                                if (lhs.h_ < rhs.h_) {
                                        return true;
                                }
                                else {
                                        return false;
                                }
                        }
                        else{
                                return false;

                        }
                }
                else {
                        return false;
                }
        };
        friend ostream& operator<<(ostream& out, Box& B)
        {
                return out << B.l_ << ' ' << B.b_ << ' ' << B.h_;
        };
private:
        int l_, b_, h_;
};

void check2()
{
        int n;
        cin>>n;
        Box temp;
        for(int i=0; i<n; i++)
        {
                int type;
                cin>>type;
                if(type ==1)
                {
                        cout<<temp<<endl;
                }
                if(type == 2)
                {
                        int l,b,h;
                        cin>>l>>b>>h;
                        Box NewBox(l,b,h);
                        temp=NewBox;
                        cout<<temp<<endl;
                }
                if(type==3)
                {
                        int l,b,h;
                        cin>>l>>b>>h;
                        Box NewBox(l,b,h);
                        if(NewBox<temp)
                        {
                                cout<<"Lesser\n";
                        }
                        else
                        {
                                cout<<"Greater\n";
                        }
                }
                if(type==4)
                {
                        cout<<temp.CalculateVolume()<<endl;
                }
                if(type==5)
                {
                        Box NewBox(temp);
                        cout<<NewBox<<endl;
                }
        }
}

int main()
{
        check2();
}
