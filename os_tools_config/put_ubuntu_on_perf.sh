# Install cpufrequtils:

sudo apt-get install cpufrequtils -y

# Then edit the following file (if it doesn't exist, create it):

sudo touch /etc/default/cpufrequtils

# And add the following line to it:

sudo echo GOVERNOR=\"performance\" >> /etc/default/cpufrequtils

# Now you need to disable ondemand daemon, otherwise after you reboot the settings will be overwritten.

sudo update-rc.d ondemand disable

# Reboot for the new configuration to kick in:

sudo reboot

# You can check your settings with:

cpufreq-info
