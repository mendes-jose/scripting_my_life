# How to fix dates of old commits

Find out what commits are wrong and define new dates:

```sh
git log --after="2011-11-11" --until="2013-11-12" --pretty-fuller # get a list of commits with the desired dates
git show <commit-sha>^1 # see the commit before a given commit - to determine the "right" date
```

Than edit the following example in order to change the commits:

```sh
git filter-branch -f --env-filter \
    'if [ $GIT_COMMIT = eafda78b6ea1c30f98f39116c240dc5d572164b9 ]
    then
      export GIT_AUTHOR_DATE="Tue Feb 13 10:17:42 2018 +0100"
      export GIT_COMMITTER_DATE="Tue Feb 13 10:17:42 2018 +0100"
    elif [ $GIT_COMMIT = e7f77c4d1a1d2e12b2629c01ee6e6eccdc180652 ]
    then
      export GIT_AUTHOR_DATE="Thu Nov 9 14:57:41 2017 +0100"
      export GIT_COMMITTER_DATE="Thu Nov 9 14:57:41 2017 +0100"
    elif [ $GIT_COMMIT = aa87240d012c1ad0e2ac10bc6ae732a7d653bfef ]
    fi'
```

Since a change of a commit like this invalidates all future commits' hashes, its best to change them all at once (using if then elif as shown).
Otherwise you'll have to redo the first steps.

If you want to push to a remote use `--force` option. But it is best not to merge. A new clone is better.