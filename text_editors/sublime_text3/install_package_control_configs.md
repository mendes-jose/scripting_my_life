Install Package Control
=======================

The simplest method of installation is through the Sublime Text console. The console is accessed via the ctrl+` shortcut or the View > Show Console menu. Once open, paste the appropriate Python code for your version of Sublime Text into the console.

```python
import urllib.request,os,hashlib; h = '2915d1851351e5ee549c20394736b442' + '8bc59f460fa1548d1514676163dafc88'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```

This code creates the Installed Packages folder for you (if necessary), and then downloads the Package Control.sublime-package into it. The download will be done over HTTP instead of HTTPS due to Python standard library limitations, however the file will be validated using SHA-256.

DocBlockr
--------

Do /**[tab]

DocBlockr_Python
----------------
  
To get started:

1. Open a python file (syntax must be currently set to python)
2. Open a double quoted docstring (""") at one of the following places:
- Top of the file
- Beginning of a class
- Beginning of a function
3. Press [enter] to trigger the autocomplete

If you don't want to trigger the parser on a docstring, simply hold down [ctrl]/[cmd] and press [enter]

For configuration and extendability, see the readme
https://github.com/adambullmer/sublime_docblockr_python

Examples

1.
class TestClass():
  """<-- Press [enter]

2.
def test_function():
  """<-- Press [enter]

GIT
---

Git helps you interact with your Git repo. It has support for all sorts of things like init, push, pull, branch, stash, and more. Read more on how exactly you can [use Git inside of Sublime text to improve your workflow](https://scotch.io/tutorials/using-git-inside-of-sublime-text-to-improve-workflow)

GITGUTTER
---------

This is a small, but useful plugin that will tell you what lines have changed since your last Git commit. An indicator will show in the gutter next to the line numbers.


DISTRACTION FREE MODE Settings
------------------------------

```json
{
    "line_numbers": true,
    "gutter": true,
    "draw_centered": true,
    "wrap_width": 120,
    "word_wrap": true,
    "scroll_past_end": true
}
```

Key Bindings
------------

```json
[
	{ "keys": ["ctrl+;"], "command": "toggle_comment", "args": { "block": false } },
	{ "keys": ["ctrl+shift+;"], "command": "toggle_comment", "args": { "block": true } },
	{ "keys": ["ctrl+/"], "command": "show_overlay", "args": {"overlay": "goto", "text": "#"} },
]
```