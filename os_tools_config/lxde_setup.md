## How it looks

<!--![alt text](./desktop.png)-->

<h1 align="center">
  <img src="./desktop.png" alt="Desktop" width="50%">
</h1>


## Theme

[Arc theme]([https://github.com/horst3180/arc-them) is great!

```sh
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_16.04/ /' > /etc/apt/sources.list.d/home:Horst3180.list"
sudo apt-get update
sudo apt-get install arc-theme
```

To justify titles to the left edit `~/.themes/Arc-Darker/openbox-3/themerc` so it has the following line `window.active.text.justify: left`

## Icons

[Vibrancy](http://www.ravefinity.com/p/vibrancy-colors-gtk-icon-theme.html)

```sh
sudo add-apt-repository ppa:ravefinity-project/ppa
sudo apt-get update
sudo apt-get install vibrancy-colors
```

## Windows titles (and buttons)

Edit `~/.config/openbox/lxde-rc.xml` or `~/.config/openbox/lubuntu-rc.xml` and chat `theme.titleLayout` to `L` or `CMIL`.

Edit value of field `window.label.text.justify` in `~/.themes/Arc-Darker/openbox-3/themerc` to `left`.

## Cursors

Hackneyed

## Fonts

Install `noto sans` (google it)

## Wallpaper

Search for flat wallpaper on the internet and put in `/usr/share/lubuntu/wallpapers`

#### Greeter (Login screen)

Edit greeter.background in `/etc/lightdm/lightdm-gtk-greeter.conf` with absolute path.

## Notification

Not working, forget the name of the replacement of xfce4-notify

```sh
notify-send 'Battery Low!' 'You have only 5 minutes left' --icon=/usr/share/icons/Vibrancy-Colors-Dark/status/48/battery_caution.png
```