#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <map>
#include <deque>
#include <sstream>

using namespace std;


typedef struct TagContent
{
        vector< pair<string, string> > attributes; // dictionary of attributes keys and its values
        vector< pair<string, TagContent> > tags; // nested tags
} TagContent;

template <class Container>
void split_str(const std::string& str, Container& cont, char delim = ' ')
{
        std::stringstream ss(str);
        std::string token;
        while (std::getline(ss, token, delim))
        {
                cont.push_back(token);
        }
}

pair<string, TagContent> parse_tag(size_t *n_lines, string first_line = "")
{
        string str;
        // pair<string, TagContent> resp;

        string tag_name;
        TagContent tag_content;

        if (first_line == "")
                getline(cin, str);
        else
                str = first_line;
        if (str != "")
        {
                deque<string> words;
                split_str(str.substr(1, str.size()-2), words);
                // resp[words[0]] = tag_content;
                tag_name = words[0];
                size_t i = 1;
                while (i < words.size())
                {
                        tag_content.attributes.push_back(make_pair(words[i], words[i+2].substr(1, words[i+2].size()-2)));
                        // [words[i]] = words[i+2].substr(1, words[i+2].size()-2);
                        // tag_content.attributes[words[i]] = words[i+2].substr(1, words[i+2].size()-2);
                        i += 3;
                }
        }
        else
        {
                throw "Cannot getline!";
        }

        while(true)
        {
                if(getline(cin, str))
                {
                        // string aux("</" + tag_name + ">");
                        if(str == "</" + tag_name + ">")
                        {
                                *n_lines += 2;
                                break;
                        }
                        else
                        {
                                tag_content.tags.push_back(parse_tag(n_lines, str));
                        }
                }
                else
                {
                        throw "Cannot getline!";
                }
        }
        return make_pair (tag_name, tag_content);
}

template <class T>
int find_in_vec (string key, vector< pair<string, T> > source)
{
        for (auto i = 0; i < source.size(); ++i)
        {
                if (source[i].first == key)
                        return i;
        }
        return -1;
}

void print_query(string query, TagContent root)
{
        deque<string> list_tags;
        split_str(query, list_tags, '.');

        TagContent document = root;
        while (true)
        {
                if (list_tags.size() == 1)
                {
                        vector<string> aux;
                        split_str(list_tags[0], aux, '~');
                        string t_name(aux[0]);
                        string attr(aux[1]);

                        // for (auto el : document.tags)
                        //         cout << "         " << el.first << endl;
                        // if (document.tags.find(t_name) != document.tags.end())
                        int tag_pos = find_in_vec(t_name, document.tags);
                        if (tag_pos != -1)
                        {
                                int attr_pos = find_in_vec(attr, document.tags[tag_pos].second.attributes);
                                if (attr_pos != -1)
                                // if (document.tags[t_name].attributes.find(attr) != document.tags[t_name].attributes.end())
                                {
                                        cout << document.tags[tag_pos].second.attributes[attr_pos].second << endl;
                                        break;
                                }
                                cout << "Not Found!" << endl;
                                break;
                        }
                        cout << "Not Found!" << endl;
                        break;
                }

                int tag_pos = find_in_vec(list_tags[0], document.tags);
                if (tag_pos != -1)
                {
                        document = document.tags[tag_pos].second;
                        list_tags.pop_front();
                        continue;
                }
                cout << "Not Found!" << endl;
                break;
        }
}

// void print_nested (TagContent document, string name = "root", string space = "")
// {
//         cout << space << name << ": ";
//         for (pair<string, string>::iterator element : document.attributes);
//                 cout << element.first << " = " << element.second << ", ";
//         cout << endl;
//         for (auto element : document.tags)
//         {
//                 print_nested(element.second, element.first, space + "    ");
//         }
// }

int main() {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        size_t N, Q;
        cin >> N;
        cin >> Q;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        TagContent root;

        size_t i = 0;
        while(i < N)
        {
                root.tags.push_back(parse_tag(&i));
        }

        for(auto i = 0; i < Q; ++i)
        {
                string str;
                if(getline(cin, str))
                {
                        print_query(str, root);
                }
        }
        return 0;
}
