// Exemple with differents constructor for different messages types
class Exception : public std::exception
{
public:
        /** Constructor (C strings).
         *  @param message C-style string error message.
         */
        explicit Exception(const char* message) :
                msg_(message)
        {
        };

        /** Constructor (C++ STL strings).
         *  @param message The error message.
         */
        explicit Exception(const std::string& message) :
                msg_(message)
        {
        };

        /** Constructor (int).
         *  @param message Just an int.
         */
        explicit Exception(const int message)
        {
                msg_ = std::to_string(message);
        };

        /** Destructor.
         */
        virtual ~Exception() throw (){
        };

        /** Returns a pointer to the (constant) error description.
         *  @return A pointer to a const char*.
         */
        virtual const char* what() const throw (){
                return msg_.c_str();
        };

protected:
        /** Error message.
         */
        std::string msg_;
};

class Exception : public std::exception
{
private:
        std::string _msg;
public:
        Exception(int n) : std::exception() {
                _msg = std::to_string(n);
        };
        virtual ~Exception() throw(){
        };
        virtual const char *what() const throw()
        {
                return _msg.c_str();
        }
};
