# Making the touchpad work on a HP Pavilion Chromebook in Ubuntu 14.04 and 14.10

For Ubuntu 14.04 you have to add the following to `/etc/rc.local` before `exit 0`:

```sh
modprobe -r chromeos_laptop
modprobe i2c-i801
modprobe chromeos_laptop
```

For Ubuntu 14.10 you also have to add a "sleep 1" after loading the i2c module, so it would in this case read:

```sh
modprobe -r chromeos_laptop
modprobe i2c-i801
sleep 1
modprobe chromeos_laptop
```

Then copy `/usr/share/X11/xorg.conf.d/50-synaptics.conf` to `/etc/X11/xorg.conf.d/51-synaptics.conf` and edit it adding the following lines to the
inputclass section named "touchpad catchall":

```sh
Option "FingerHigh" "5"
Option "FingerLow" "5"
```

Reboot, and there you have it!
