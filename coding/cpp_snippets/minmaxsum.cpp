#include <climits>

// Complete the miniMaxSum function below.
void miniMaxSum(vector<int> arr) {
        const unsigned int size = 5;
        if (arr.size() != size)
        {
                std::stringstream ss;
                ss << "An array of size " << size << " is expected.";
                throw ss.str();
        }
        unsigned long long int min, max;
        // initialize
        max = 0;
        min = ULLONG_MAX;
        // ------------ alternatively:
        // min = 0;
        // for (auto el : arr)
        // {
        //     if (el < 0)
        //     {
        //         throw "Array values must all be positives!";
        //     }
        //     min += el;
        // }
        // ------------

        for (auto i = 0; i < size; ++i)
        {
                if (arr[i] < 0)
                {
                        throw "Array values must be positives!";
                }
                unsigned long long int sum = 0;
                for (auto j = 0; j < size; ++j)
                {
                        if (i == j)
                        {
                                continue;
                        }
                        sum += arr[j];
                }
                if (sum < min)
                {
                        min = sum;
                }
                if (sum > max)
                {
                        max = sum;
                }
        }
        std::cout << min << " " << max;
}
