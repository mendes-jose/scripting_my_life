# Geth + Arch Linux ARMv7 + Raspberry Pi 3 + Remote MIST


Mostly based on [this](https://pgaleone.eu/raspberry/ethereum/archlinux/2017/09/06/ethereum-node-raspberri-pi-3/) post.

- [raspbian+geth](http://raspnode.com/diyEthereumGeth.html#raspconfig)
- [testnet](http://blog.jaumesola.com/setting-up-an-ethereum-node-on-the-rinkeby-testnet)
- [testnet and multi geths](https://github.com/ethereum/go-ethereum/wiki/Connecting-to-the-network)
- [rinkeby](https://gist.github.com/cryptogoth/10a98e8078cfd69f7ca892ddbdcf26bc)



### Prepare the SD card while running Windows

If you’re using Windows, you can just:

- Download Win32 Disk Imager [here](https://sourceforge.net/projects/win32diskimager/)
- Download the Archlinux ARM SD image [here](https://sourceforge.net/projects/archlinux-rpi2/)
- Run Win32 Disk Imager. Select the right device, the .img file downloaded and `Write`.

### Resize the filesystem of the root partition

```sh
# (As root)
#
# TL;DR option:
fdisk /dev/mmcblk0
# inside fdisk prompt type:
# d <enter> 2 <enter> n <enter> p <enter> 2 <enter> <enter> <enter> w <enter>
#
# Longer version:
fdisk /dev/mmcblk0
# At the fdisk prompt, delete the partition number 2
# Type d then 2.
# Create a new primary partition (id 2)
# Type n then p.
# At the prompt for the first sector and last sector just press enter.
# This will automatically start from the old first sector and will set the last
# sector to the end of the available space.
# Write the partition table and exit by typing w.
```

Quit the prompt and:

```sh
reboot
resize2fs /dev/mmcblk0p2
```

### Wireless connection setup

```sh
cd /etc/netctl
install -m640 examples/wireless-wpa wireless-home
nano wireless-home # edit so it matches your wifi network
netctl start wireless-home
netctl enable wireless-home
reboot
```

Let's assume the raspberry ip was set to `192.168.0.30` from now on.

### Update packages

```sh
pacman -Syu
```

### Users

Install `sudo`:

```sh
pacman -S sudo
```

```sh
useradd geth
mkdir /home/geth
chown -R geth /home/geth
chgrp -R geth /home/geth
passwd geth
```

Add `geth` to sudoers list using `visudo`:

1. Login as root in your GUI or in a regular terminal when you boot up
2. As root type visudo ( i hope you know how to use vi, if not, please go look up a quick guide http://acs.ucsd.edu/info/vi_tutorial.shtml )
3. Use arrow keys to go down to the line that starts with ROOT
4. Press Y twice to "yank" the line
5. Press P once to paste the line
6. Press i once to enter insert mode
7. Delete the word root and replace it with desired user name
8. Press Esc once
9. type :x
10. type reboot

Delete alarm user:

`userdel -r alarm`


### Auto mount NTFS external drive config

Update before installing ntfs-3g

```sh
pacman -S ntfs-3g
```

Get external drive info: `blkid`

`fstab` should look like this for automatic mounting the drive at boot:

```sh
#
# /etc/fstab: static file system information
#
# <file system>	<dir>	<type>	<options>                   <dump>  <pass>
/dev/mmcblk0p1  /boot   vfat    defaults                      0        0
/dev/sda1       /mnt    ntfs-3g uid=geth,gid=geth,umask=0022  0        0
```

### Link to ethereum on external HD

```sh
ln -s /mnt/ethereum /home/geth/.ethereum
```

### Geth service

Create the service file `/etc/systemd/system/geth@.service` with the following content:

```sh
[Unit]
Description=geth Ethereum daemon
Requires=network.target

[Service]
Type=simple
User=%I
# For allowing all (that I think I would use) apis: --rpcapi="db,eth,net,web3,personal,web3"
ExecStart=/usr/bin/geth --rpc --rpcapi="db,eth,net,web3,personal,web3" --rpcaddr 192.168.0.30 --fast --cache=128
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

```sh
[Unit]
Description=swarm
Requires=network.target

[Service]
Type=simple
User=%I
ExecStart=/usr/bin/swarm -bzzaccount /home/geth/.ethereum/swarmKey
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

Enable them:

```sh
systemctl enable geth@geth.service
systemctl start geth@geth.service
systemctl enable swarm@geth.service
systemctl start swarm@geth.service
```

### Mist attached to a Geth node on a different computer over HTTP RPC

You will need to start the node with RPC enabled specifying the ip but keeping the default port `8545`:

```sh
geth --rpc --rpcapi="db,eth,net,web3,personal,web3" --rpcaddr 192.168.0.30 --fast --cache=128
swarm -bzzaccount /home/geth/.ethereum/swarmKey
```

Connect Mist on another machine in the same network via:

```sh
# mist --rpc http://192.168.0.30:8545
mist --rpc http://192.168.0.30:8545 --swarmurl="http://192.168.0.30"
```

### Geth commands

- Create new account: `geth account new`
- Use Rinkeby testnet instead of mainnet just put `--rinkeby` option
