# Magnificent [gtop](https://github.com/aksakalli/gtop)

Install nodejsf and npm

```sh
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Try installing

```sh
sudo npm install gtop -g
```