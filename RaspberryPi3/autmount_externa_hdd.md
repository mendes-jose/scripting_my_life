RaspberryPi automount external HD
=================================

```bash
sudo apt-get install ntfs-3g
ls -l /dev/disk/by-uuid/
```

write down the uuid

```bash
sudo mkdir /mnt/mybook
sudo chmod 770 /mnt/mybook
```

Get the uid, gid for pi user and group with id command (usually 1000)

```bash
sudo cp /etc/fstab /etc/fstab.backup
sudo vim /etc/fstab
```

add the line

```bash
UUID=0AC4D607C4D5F543 /mnt/mybook ntfs-3g uid=1000,gid=1000,umask=007 0 0
```

In case another user needs access, add it to group:

```bash
sudo usermod -a -G pi plex # add plex to group pi
```