# Chrony 3.3 setup tested on Ubuntu 14.04.5 (assuming `systemd` and `curl` installed)

Download and extract:

```sh
cd ~/Downloads
curl -O https://download.tuxfamily.org/chrony/chrony-3.3.tar.gz
tar -xvzf chrony-3.3.tar.gz
cd chrony-3.3
```

Build and install:

```sh
./configure --prefix=/usr
make
sudo make install
```

Set configuration files:

```sh
sudo cp ~/Downloads/chrony-3.3/examples/chrony.conf.example1 /etc/chrony.conf
sudo cp ~/Downloads/chrony-3.3/examples/chronyd.service /etc/systemd/system/
```

Enable running at startup and run:

```sh
sudo systemctl --system daemon-reload
sudo systemctl enable chronyd.service
sudo systemctl start chronyd.service
```