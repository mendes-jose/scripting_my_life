Taken mostly from [here](https://gist.github.com/renshuki/3cf3de6e7f00fa7e744a)
===============================================================================

Install zsh
-----------

```bash
sudo apt-get install zsh git curl -y
```

Install Oh My ZSH
-----------------

```bash
cd
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

Install missing fonts (powerline)
---------------------------------

```bash
cd
git clone https://github.com/powerline/fonts.git
mv fonts .fonts
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf
mv PowerlineSymbols.otf ~/.fonts/
mkdir -p .config/fontconfig/conf.d
```

Clean cache
-----------

```bash
fc-cache -vf ~/.fonts/
```

Move config file
----------------

```bash
mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/
```

Config zsh
----------

### Change theme from default to agnoster

```bash
sed -i 's/robbyrussell/agnoster/g' ~/.zshrc
```

### Change terminal colorscheme (I did manualy)

### Make sure that character encoding is UTF-8

Change directory colors to solarized
------------------------------------

```bash
mkdir ~/.solarized
wget https://raw.githubusercontent.com/seebi/dircolors-solarized/master/dircolors.ansi-dark
mv dircolors.ansi-dark .solarized
echo "eval \`dircolors ~/.solarized/dircolors.ansi-dark\`" >> ~/.zshrc
```

Set default shell to zsh
------------------------

```bash
chsh -s $(which zsh)
```

Restart system
--------------

```bash
sudo shutdown 0
```
