# Solution 1:

Add this line to the file CMakeLists.txt:

```
set(CMAKE_BUILD_TYPE Debug)
```

# Solution 2:

compile in Release mode optimized but adding debug symbols, useful for profiling :

```
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ...
```

or compile with NO optimization and adding debug symbols :

```
cmake -DCMAKE_BUILD_TYPE=Debug ...
```

# Solution 3:

```
IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)
```