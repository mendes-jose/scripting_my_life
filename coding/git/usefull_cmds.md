Remove all deleted files
------------------------

```sh
git rm $(git ls-files --deleted)
```

Tree-like view of commits in terminal
-------------------------------------

```sh
git log --graph --pretty=oneline --abbrev-commit
```

Or even better:

```sh
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
```
