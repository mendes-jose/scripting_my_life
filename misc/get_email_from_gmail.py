#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import imaplib
import getpass
import email
import datetime
import re
import urllib2
import json
import quopri
import unicodedata
import datetime
import csv
import matplotlib.pyplot as plt

""" Delivery email example (french version)
--------------------------------------------------
Commande #0000 Livrée
--------------------------------------------------

Livrée à:           2015-10-27 21:50:21
Temps de livraison: 6 min 9 s

Restaurant:         Lotus Blanc
Adresse restaurant: 45 rue de Bourgogne, Paris, 75007
Tél. restaurant:    +33145551889

Client:             Pierre Smith
Adresse client:     00 Rue de Varenne, Paris, 75007
Tél. client:        0555555555
Tél. client 2:      0555555555

Pourboire CB:       0.00
"""

def process_delivery_emails(M):
	# get emails IDs
	#rv, msgnums = M.search(None, '(FROM noreply@deliveroo. co.uk SUBJECT "Command #")')
	rv, msgnums = M.search(None, '(FROM noreply@deliveroo.co.uk SUBJECT "Command #")')
	#print msgnums
	if rv != 'OK':
		print "No delivery emails found!"
		return

	try:
		with open('/storage/emulated/0/com.hipipal.qpyplus/scripts/table.csv', 'rb') as csvfile:
			treader = csv.reader(csvfile, delimiter='|')
			#treader = csv.reader(csvfile, delimiter='|',quotechar='',quoting=csv.QUOTE_NONNUMERIC)
			tlist = list(treader)
			hdr = tlist[0]
			table = tlist[1:]
	except:
		with open('/storage/emulated/0/com.hipipal.qpyplus/scripts/table.csv', 'w') as csvfile:
			hdr = ['ID', 'T', 'D', 'R', 'AR', 'TR', 'C', 'AC', 'TC1', 'TC2', 'P', 'GP', 'GT', 'GE']
			table_writer = csv.writer(csvfile, delimiter='|')
			#table_writer = csv.writer(csvfile, delimiter='|',quotechar='', quoting=csv.QUOTE_NONNUMERIC)
			table_writer.writerow(hdr)
			table = []

	for num in msgnums[0].split():
		rv, data = M.fetch(num, '(RFC822)')
		if rv != 'OK':
			print "ERROR getting email #", num
			return
		
		# quopri decode MINE quoted-printable data
		msg = email.message_from_string(quopri.decodestring(data[0][1]))
		del_info = msg.get_payload()

		m = re.search(r'Commande\s*#([0-9]*)\s*Livrée', del_info)
		n_command_value = int(m.group(1))
		n_command_str = m.group(1)

		#print '---------------', n_command_value, '---------------'

		m = re.search(r'Livrée\s*à:\s*([0-9]*)-([0-9]*)-([0-9]*)\s*([0-9]*):([0-9]*):([0-9]*)', del_info)
		del_moment = datetime.datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)))

		nrows = len(table)
		if table != []:
			ids_list = [int(table[i][hdr.index('ID')]) for i in range(nrows)]
			#print ids_list
		else:
			ids_list = []

		idx1 = [i for i, x in enumerate(ids_list) if x == n_command_value]
		#print idx1

		if idx1 != []:
			date_list = [datetime.datetime(*[int(a) for a in re.split('-| |:', table[i][hdr.index('T')])]) for i in range(nrows)]
			idx2 = [i for i, x in enumerate(date_list) if x == del_moment]
			#print idx2
			if idx2 != []:
				continue

		m = re.search(r'Temps de livraison:\s*([0-9]*)\s*min\s*([0-9]*)\s*s', del_info)
		#accum_del_time += int(m.group(1))*60 + int(m.group(2))
		del_duration = int(m.group(1))*60 + int(m.group(2))

		m = re.search(r'Pourboire CB:\s*([0-9]*)\.([0-9]*)', del_info)
		del_tips = float(m.group(2))/100.+float(m.group(1))

		m = re.search(r'Restaurant:\s*(.*)\r', del_info)
		del_rest = m.group(1)

		m = re.search(r'Adresse restaurant:\s*(.*)\r', del_info)
		rest_addr = m.group(1) # remove \r

		m = re.search(r'Tél. restaurant:\s*([0-9+]*)\r', del_info)
		rest_tel = m.group(1)

		m = re.search(r'Client:\s*(.*)\r', del_info)
		del_clnt = m.group(1)

		m = re.search(r'Tél. client:\s*([0-9+]*)\r', del_info)
		clnt_tel = m.group(1)

		m = re.search(r'Tél. client 2:\s*([0-9+]*)\r', del_info)
		clnt_tel2 = m.group(1)

		m = re.search(r'Adresse client:\s*(.*)\r', del_info)
		clnt_addr = m.group(1) # remove \r

		#call google maps api to cal distance and estimated time between address

		base_url = 'https://maps.googleapis.com/maps/api/directions/json?'
		api_key = 'AIzaSyAPQW4xO9D94A2yAIAdG9JGRjC9-Vtle7A'

		addr_start = rest_addr.replace(" ", "+")
		addr_end = clnt_addr.replace(" ", "+")

		# remove accents
		addr_start = unicodedata.normalize('NFKD', addr_start.decode('utf-8')).encode('ascii', 'ignore')
		addr_end = unicodedata.normalize('NFKD', addr_end.decode('utf-8')).encode('ascii', 'ignore')

		#addr_start = addr_start[0:-1]
		#addr_end = addr_end[0:-1]

		final_url = base_url+'origin='+addr_start+'&destination='+addr_end+'&mode=bicycling&key='+api_key

		response = urllib2.urlopen(final_url)

		gresp = json.loads(response.read())

		try:
			gdist = gresp['routes'][0]['legs'][0]['distance']['value']
			gtime = gresp['routes'][0]['legs'][0]['duration']['value']
			#print 'estimated time:', gtime
			gpossible = True
		except IndexError:
			#gnum -= 1
			gpossible = False
			gdist = 0
			gtime = 0
			print 'Error using google maps api for estimating distance and time'
		
		#add to table
		newline = [n_command_value, str(del_moment), del_duration, del_rest, rest_addr,  rest_tel, del_clnt, clnt_addr, clnt_tel,
					clnt_tel2, del_tips, gpossible, gtime, gdist]
		#print table

		#ncols = len(table[0])
		#if ncols != len(newline):
		#	table = [newline]
		#else:
		table += [newline]
			#table = np.vstack((table, np.array([newline])))
		with open('/storage/emulated/0/com.hipipal.qpyplus/scripts/table.csv', 'a') as csvfile:
			table_writer = csv.writer(csvfile, delimiter='|')
			#table_writer = csv.writer(csvfile, delimiter='|',quotechar='', quoting=csv.QUOTE_NONNUMERIC)
			table_writer.writerow(newline)

def print_sumary():
	with open('/storage/emulated/0/com.hipipal.qpyplus/scripts/table.csv', 'rb') as csvfile:
			treader = csv.reader(csvfile, delimiter='|')
			#treader = csv.reader(csvfile, delimiter='|',quotechar='',quoting=csv.QUOTE_NONNUMERIC)
			tlist = list(treader)
			hdr = tlist[0]
			table = tlist[1:]

	total_orders = len(table)
	print '{0:48} {1:16}'.format(
			'Total number of deliveries:',
			total_orders)
	#print 'All del times:', [table[i][hdr.index('D')] for i in total_orders]
	total_time = sum([int(tim) for tim in [table[i][hdr.index('D')] for i in range(total_orders)]])
	print '{0:48} {1:4} h {2:2} m {2:2} s'.format(
			'Total delivery time:',
			int(total_time/3600),
			int(total_time/60) - int(total_time/3600)*3600/60,
			total_time - int(total_time/60)*60)

	tavg_sec = int(round(total_time/float(total_orders)))
	print '{0:48}        {1:2} m {2:2} s'.format(
			'Average delivery time:',
			int(tavg_sec/60),
			tavg_sec - int(tavg_sec/60)*60)

	gtotal_time = sum([int(tim) for tim in [table[i][hdr.index('GT')] for i in range(total_orders)]])

	gp_list = [bool(table[i][hdr.index('GP')]) for i in range(total_orders)]
	gp_idx = [i for i, x in enumerate(gp_list) if x == True]

	gtotal = len(gp_idx)

	print '{0:48} {1:4} h {2:2} m {2:2} s'.format(
			'Total delivery time (by google maps api):',
			int(gtotal_time/3600),
			int(gtotal_time/60) - int(gtotal_time/3600)*3600/60,
			gtotal_time- int(gtotal_time/60)*60)

	gtavg_sec = int(round(gtotal_time/float(gtotal)))
	print '{0:48}        {1:2} m {2:2} s'.format(
			'Average arrival time (by google maps api):',
			int(gtavg_sec/60),
			gtavg_sec - int(gtavg_sec/60)*60)

	gtotal_dist = sum([int(dist) for dist in [table[i][hdr.index('GE')] for i in range(total_orders)]])

	print '{0:48} {1:14} m'.format(
			'Total delivery distance (by google maps api):',
			gtotal_dist)

	gdavg = int(round(gtotal_dist/float(gtotal)))
	print '{0:48} {1:14} m'.format(
			'Average arrival distance (by google maps api):',
			gdavg)

	total_tips = sum([float(tip) for tip in [table[i][hdr.index('P')] for i in range(total_orders)]])
	print '{0:48} {1:12.2} EUR'.format(
			'Total tips by card:',
			total_tips)

	print '{0:48} {1:12.2} EUR'.format(
			'Average tip by card:',
			total_tips/total_orders)

	print ''

	orders_date = [table[i][hdr.index('T')] for i in range(len(table))]
	sorted_orders_idx = sorted(range(len(orders_date)), key=orders_date.__getitem__)

	fig = plt.figure()
	fig2 = plt.figure()
	ax = fig.gca()
	ax.grid(True)
	ax2 = fig2.gca()
	ax2.grid(True)
	plt.hold(True)

	x_del = range(total_orders)
	my_time = [int(table[i][hdr.index('D')])/60. for i in sorted_orders_idx]
	google_time = [int(table[i][hdr.index('GT')])/60. for i in sorted_orders_idx]
	google_dist = [int(table[i][hdr.index('GE')])/100. for i in sorted_orders_idx]
	#i_google_time = linear_interp(google_time, 100)
	i_google_time = google_time
	print len(google_time), len(i_google_time)
	#i_my_time = linear_interp(my_time, 100)
	i_my_time = my_time
	print len(my_time), len(i_my_time)
	#i_x_del = linear_interp(x_del, 100)
	i_x_del = x_del
	print len(i_x_del)
	#print i_my_time, i_google_time
	#print len(x_del), len(my_time), len(google_time), len(google_dist)

	ax.plot(x_del, my_time, label='My time', marker='o', color='blue')
	ax.plot(x_del, google_time, label='Google\'s time', marker='o', color='red')

	"""
	ax.fill_between(i_x_del, 0, [min(i,j) for i, j in zip(i_my_time, i_google_time)], alpha=0.3, facecolor='red')
	ax.fill_between(i_x_del,
			i_my_time,
			i_google_time,
			where=[i <= j for i, j in zip(i_my_time, i_google_time)],
			facecolor='red',
			alpha=0.3,
			interpolate=True)
	ax.fill_between(i_x_del,
			i_my_time,
			i_google_time,
			where=[i >= j for i, j in zip(i_my_time, i_google_time)],
			facecolor='blue',
			alpha=0.3,
			interpolate=True)
	"""
	ax.fill_between(i_x_del,
			0,
			i_my_time,
			facecolor='blue',
			alpha=0.1,
			interpolate=True)
	ax.fill_between(i_x_del,
			0,
			i_google_time,
			facecolor='red',
			alpha=0.1,
			interpolate=True)
	ax.set_xlim([0, len(i_x_del)-1])
	ax.set_ylim([0, 25])
	#ax.set_ylim([min(my_time), 1.1*max(my_time)])

	"""
	ax.fill_between(range(total_orders),
			0,
			[int(table[i][hdr.index('GT')]) for i in sorted_orders_idx],
			alpha=0.15,
			facecolor='red')
	"""
	ax2.plot(x_del, google_dist, label='Google\'s distance', marker='o', color='red')

	ax2.fill_between(x_del, 0, google_dist, alpha=0.5, facecolor='red')

	#ax.plot(range(total_orders), table[sort_orders_idx, hdr.index('GT')], marker='--')
	ax.set_xlabel('Deliveries')
	ax.set_ylabel('Time (s)')
	ax2.set_xlabel('Deliveries')
	ax2.set_ylabel('Distance (m)')
	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, loc=2)
	handles, labels = ax2.get_legend_handles_labels()
	ax2.legend(handles, labels, loc=2)
	fig.savefig('./time_per_delivery.png', bbox_inches='tight', dpi=300)
	fig2.savefig('./distance_per_delivery.png', bbox_inches='tight', dpi=300)
	plt.show()
	
def 	frange(initial, final, step):
	""" Float point range function with round at the int(round(1./step))+4 decimal position
	"""
	_range = []
	n = 0
	if initial == final:
		return [initial]*int(round(1./step))
	elif initial < final:
		while n*step+initial < final:
			_range += [round(n*step+initial, 4+int(round(1./step)))]
			n+=1
	else:
		while n*step+initial > final:
			_range += [round(n*step+initial, 4+int(round(-1./step)))]
			n+=1
	return _range

def linear_interp(l, n):
	new_list = [frange(i, j, (j-i)/float(n)) for i, j in zip(l[0:-1], l[1:])]
	#print new_list
	new_list = [item for sublist in new_list for item in sublist]
	new_list += [float(l[-1])]
	return new_list

M = imaplib.IMAP4_SSL('imap.gmail.com')

for i in range(3):
	try:
	    M.login('jmagno.mf', getpass.getpass())
	    break
	except imaplib.IMAP4.error:
	    print "LOGIN FAILED!!! Try again"
	    continue
    	# ... exit or deal with failure...

if i == 2:
	quit()

rv, data = M.select()
if rv == 'OK':
	print "Processing emails...\n"

	process_delivery_emails(M)

	print_sumary()
	
	M.close()
M.logout()