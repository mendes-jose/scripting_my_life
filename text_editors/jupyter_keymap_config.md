# Jupyter keymap configuration

I've been using sublime text editor for code projects for a while. Whilst jupyter notebooks is great for data science projects, I was missing some of those useful shortcuts like adding lines above and below, editing variables, multiple cursors etc.
For those who use sublime you know what I mean.
Well there is a way to get some of that mojo back!! Sublime editing in a Jupyter cell!!!
What it does is allow you do a bunch of Sublime Text Commands in a jupyter cell.
I’m assuming you need to have sublime text 3 installed on your system, then follow these instructions:
So, how do you do this? It’s really simple.

Find your Jupyter configuration folder by running `jupyter --config-dir`.
create a folder under this called `custom` and create `custom.js` file in the custom sub-folder in your favourite editor.

Add the following lines to the bottom of the file:

```js
require(["codemirror/keymap/sublime", "notebook/js/cell", "base/js/namespace"],
    function(sublime_keymap, cell, IPython) {
        // setTimeout(function(){ // uncomment line to fake race-condition
        cell.Cell.options_default.cm_config.keyMap = 'sublime';
        var cells = IPython.notebook.get_cells();
        for(var cl=0; cl< cells.length ; cl++){
            cells[cl].code_mirror.setOption('keyMap', 'sublime');
        }
 
        // }, 1000)// uncomment  line to fake race condition 
    } 
);
```

It brings some nice tools to editing in a notebook cell. If you run into issues, like installation or keystroke clashes, start by reading the comments below the post16.

It seems to only work in the current editable cell, not across the notebook. You can always join, edit and split cells as a work around.