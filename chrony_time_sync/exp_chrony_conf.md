# Master Alfa

Configuration for the master that sometimes connects to internet (and gets accurate time) and sometimes goes online in the experiment network.
Let's assume its IP in the experiment network is `10.42.0.1`.

```
pool pool.ntp.org iburst
makestep 1.0 3
local stratum 8
allow 10.42.0
rtcsync
```

# Master Beta

Configuration for the secondary master that serves all machines in the experiment network but will have its time changed by Master Alfa when/if it is present.
Let's assume its IP is `10.42.0.2`.

```
server 10.42.0.1
server 10.42.0.5
driftfile /var/lib/chrony/drift
logdir /var/log/chrony
log measurements statistics tracking
local stratum 9
initstepslew 10 10.42.0.3
allow 10.42.0.3
makestep 0.5 -1
```

# Others

Let's assume this instance of "Others" has the IP `10.42.0.3`.

```
server 10.42.0.1
server 10.42.0.5
driftfile /var/lib/chrony/drift
logdir /var/log/chrony
log measurements statistics tracking
local stratum 9
initstepslew 10 10.42.0.3
allow 10.42.0.3
makestep 0.5 -1
```
