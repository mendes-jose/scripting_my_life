#!/bin/bash

# https://www.tldp.org/LDP/abs/html/comparison-ops.html

read X
read Y
if [[ "$X" -eq "$Y" ]]; then
        echo "X is equal to Y"
elif [[ "$X" -gt "$Y" ]]; then
        echo "X is greater than Y"
else
        echo "X is less than Y"
fi
