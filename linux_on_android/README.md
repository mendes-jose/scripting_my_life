# Linux on Android

- Install Termux App (Volume Up+Q → Show extra keys view)

- Install OpenSSH

    ```bash
    pkg install openssh
    pkg install vim
    sshd
    ```
- Copy keys to ~/.ssh/authorized_keys
    
    ```bash
    scp mnds@<ip-pc>:~/.ssh/id_rsa.pub
    cat id_rsa.pub >> ~/.ssh/authorized_keys
    ```

- SSH to cellphone

    ```bash
    ssh -p 8022 <ip-cellphone>
    ```



```bash
pkg install wget proot
mkdir -p ~/jails/ubuntu
cd ~/jails/ubuntu
wget https://raw.githubusercontent.com/Neo-Oli/termux-ubuntu/master/ubuntu.sh
bash ubuntu.sh
```