#!/bin/bash
expres="scale=5; ("

read N
# echo $N

i=1
while [ "$i" -le "$((N-1))" ]
do
    # echo $i
    read e
    expres="${expres}${e} + "
    i=$((i+1))
done

read e
expres="${expres}${e})/${N}"
# echo $expres
#
printf "%.3f" "$(echo "$expres" | bc)"
