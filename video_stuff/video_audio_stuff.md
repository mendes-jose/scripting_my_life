## Speeding up/slowing down video

You can change the speed of a video stream using the `setpts` video filter. Note that in the following examples, the audio stream is not changed, so it should ideally be disabled with `-an`.

To double the speed of the video, you can use:
```sh
ffmpeg -i input.mkv -filter:v "setpts=0.5*PTS" output.mkv
```

The filter works by changing the presentation timestamp (PTS) of each video frame. For example, if there are two succesive frames shown at timestamps 1 and 2, and you want to speed up the video, those timestamps need to become 0.5 and 1, respectively. Thus, we have to multiply them by 0.5.

Note that this method will drop frames to achieve the desired speed. You can avoid dropped frames by specifying a higher output frame rate than the input. For example, to go from an input of 4 FPS to one that is sped up to 4x that (16 FPS):
```sh
ffmpeg -i input.mkv -r 16 -filter:v "setpts=0.25*PTS" output.mkv
```

To slow down your video, you have to use a multiplier greater than 1:
```sh
ffmpeg -i input.mkv -filter:v "setpts=2.0*PTS" output.mkv
```

Speeding up/slowing down audio

You can speed up or slow down audio with the `atempo` audio filter. To double the speed of audio:

```sh
ffmpeg -i input.mkv -filter:a "atempo=2.0" -vn output.mkv
```

The atempo filter is limited to using values between 0.5 and 2.0 (so it can slow it down to no less than half the original speed, and speed up to no more than double the input). If you need to, you can get around this limitation by stringing multiple atempo filters together. The following with quadruple the audio speed:
```sh
ffmpeg -i input.mkv -filter:a "atempo=2.0,atempo=2.0" -vn output.mkv
```

Using a complex filtergraph, you can speed up video and audio at the same time:
```sh
ffmpeg -i input.mkv -filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]" -map "[v]" -map "[a]" output.mkv
```

## Cutting

Cut the 48 first seconds of a video

Faster:
```sh
ffmpeg -i input.mp4 -ss 00:00:00 -t 00:00:48 -c copy 1 output.mp4
```

```sh
avconv  -i rviz_n.flv -ss 00:00:15,490 -t 00:00:08,008 -strict experimental rviz_n_t.mov
```

Slower:
```sh
ffmpeg -i input.mp4 -ss 00:00:00 -t 00:00:48 -async 1 output.mp4
```

## Images to video

### Windows
```sh
ffmpeg -framerate 290/15 -i multirobot-path-%d.png -c:v libx264 -vf "scale=-1:-1" -r 30 -pix_fmt yuv420p out.mp4
```

### Ubuntu
```sh
avconv -framerate 10 -f image2 -i multirobot-path-%d.png -c:v h264 -vf "scale=718:-1" -crf 1 out.mov
```

```sh
avconv -f image2 -framerate 275/8.008003703204453 -i paths%03d.png -c:v h264 -vf "scale=654:-1" -crf 1 plot.mov
```

## Concatenate same codes (more flexible solution)

```sh
for f in ./*.MOV; do echo "file '$f'" >> mylist.txt; done
ffmpeg -f concat -safe 0 -i mylist.txt -c copy output.MOV
```

## Concatenate diff codecs

```sh
ffmpeg -i intro_multi.mp4 -i multi.mp4 -i ctrl.mp4 -i ctrlsim.mp4 -filter_complex "[0:v:0] [1:v:0] [2:v:0] [3:v:0] concat=n=4:v=1 [v]" -map "[v]" sim_final_test.mp4
```

## Cropping

```sh
avconv -i rviz_n_t_s.mov -vf "crop=in_w*0.7:in_h:0:0"  -strict experimental rviz_t_s_c.mov
```

## Overlaying

```sh
avconv -i gopro_n_t.mov -i plot.mov -i rviz_n_t_s_c.mov -filter_complex 'overlay=W-w-20:H-h-20,overlay=20:20' -strict experimental final.mov
```

## Gif

```sh
avconv -framerate 0.5 -i steps%d.jpg -vf "scale=800:-1"  -pix_fmt rgb24 -r 0.5  steps.gif
```

